const express = require("express");
const cors = require("cors");

const app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));



// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to bezkoder application." });
});
const ledger = require("./controller/ledger.controller.js");
const external_ledger = require("./controller/external_ledger.controller.js");
const send_fund = require("./controller/send_fund.controller.js");


/* This is creating a route for the ledger.create function. */
/* 
  structure of request
  {
    "from_wallet":"jfbadblfabldsjbflkjabslkdjf",
    "to_wallet":"jfbadblfabldsjbflkjabslkdjf",
    "chain":"ETH",
    "token":"jfbadblfabldsjbflkjabslkdjf",
    "amount":"40$",
    "type":"DIRECT_DEPOSIT"
  }
*/
app.post("/ledger", ledger.create);
/* This is a route that will return all the ledgers in the database. */
app.get("/ledger", ledger.findAll);

/* This is a route that allows us to create a new external ledger. */
/* 
  structure of request
  {
    "circle_id":"jbflkjabslkdjf",
    "to_wallet":"jfbadblfabldsjbflkjabslkdjf",
    "chain":"ETH",
    "token":"jfbadblfabldsjbflkjabslkdjf",
    "amount":"40$",
    "type":"THROUGH_CIRCLE"
  }
*/
app.post("/external_ledger", external_ledger.create);
/* This is a route that will call the external_ledger.findAll function. */
app.get("/external_ledger", external_ledger.findAll);

/* This is a route that allows the user to send funds to another user. */
/* 
  structure of request
  {
    "from_wallet":"1000579008",
    "to_bank":"a2206af5-6da3-493a-a0ca-743708842338",
    "amount": 0.20,
    "email":"haha@gmail.com"
  }
*/
app.post("/send_fund", send_fund.create);
/* This is a route that will be used to send funds from one account to another. */
app.get("/send_fund", send_fund.findAll);


// require("./routes/ledger.routes.js")(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});