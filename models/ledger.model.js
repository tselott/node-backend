const sql = require("./db.js");

// constructor
const Ledger = function(ledger) {
  this.user_id = ledger.user_id;
  this.from_wallet = ledger.from_wallet;
  this.to_wallet = ledger.to_wallet;
  this.chain = ledger.chain;
  this.token = ledger.token;
  this.amount = ledger.amount;
  this.type = ledger.type;
  this.create_time = ledger.create_time;
  this.modify_time = ledger.modify_time;
  
};

Ledger.createTransaction = (data, result) => {
  sql.query("INSERT INTO ledger SET ?", data, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created data: ", { id: res.insertId, ...data });
    result(null, { id: res.insertId, ...data });
  });
};

Ledger.getAllTransactions = (result) => {
  let query = "SELECT * FROM ledger";

  sql.query(query, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("all ledgers: ", res);
    result(null, res);
  });
};
module.exports = Ledger;