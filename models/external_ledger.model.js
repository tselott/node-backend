const sql = require("./db.js");

// constructor
const ExtnalLedger = function(extnalLedger) {
  this.user_id = extnalLedger.user_id;
  this.circle_id = extnalLedger.circle_id;
  this.to_wallet = extnalLedger.to_wallet;
  this.chain = extnalLedger.chain;
  this.token = extnalLedger.token;
  this.amount = extnalLedger.amount;
  this.type = extnalLedger.type;
  this.create_time = extnalLedger.create_time;
  this.modify_time = extnalLedger.modify_time;
  
};

ExtnalLedger.createTransaction = (data, result) => {
  sql.query("INSERT INTO external_ledger SET ?", data, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created data: ", { id: res.insertId, ...data });
    result(null, { id: res.insertId, ...data });
  });
};

ExtnalLedger.getAllTransactions = (result) => {
  let query = "SELECT * FROM external_ledger";

  sql.query(query, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("all data: ", res);
    result(null, res);
  });
};

module.exports = ExtnalLedger;
