const sql = require("./db.js");

// constructor
const SendFundToBank = function(sendFund) {
  this.transaction_id = sendFund.transaction_id;
  this.from_wallet = sendFund.from_wallet;
  this.to_bank = sendFund.to_bank;
  this.bank_account_details = sendFund.bank_account_details;
};

SendFundToBank.createTransaction = (data, result) => {
    sql.query("INSERT INTO withdraw_to_bank SET ?", data, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      console.log("created data: ", { id: res.insertId, ...data });
      result(null, { id: res.insertId, ...data });
    });
  };
  SendFundToBank.getAllTransactions = (result) => {
    let query = "SELECT * FROM withdraw_to_bank";
  
    sql.query(query, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("all ledgers: ", res);
      result(null, res);
    });
  };


module.exports = SendFundToBank;
