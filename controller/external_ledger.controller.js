const { v4: uuidv4 } = require('uuid');
const ExternalLedger = require('../models/external_ledger.model')


exports.create = (req, res) => {
    if (!req.body) {
        res.status(400).send({
          message: "Content can not be empty!"
        });
    }
    const date = new Date();
    const external_ledger = new ExternalLedger({
        user_id: "1212",
        circle_id:req.body.circle_id,
        to_wallet:req.body.to_wallet,
        chain:req.body.chain,
        token:req.body.token,
        amount:req.body.amount,
        type:req.body.type,
        create_time: date,
        modify_time:req.body.modify_time,
    });
    ExternalLedger.createTransaction(
      external_ledger, (err,data) => {
            if (err)
            res.status(500).send({
              message:
                err.message || "Some error occurred while creating"
            });
          else res.send(data);
        }
    )
    
  };
  exports.findAll = (req, res) => {
  
    ExternalLedger.getAllTransactions((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving"
        });
      else res.send(data);
    });
  };