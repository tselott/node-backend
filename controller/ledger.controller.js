const Ledger = require('../models/ledger.model')


exports.create = (req, res) => {
    if (!req.body) {
        res.status(400).send({
          message: "Content can not be empty!"
        });
    }
    const date = new Date();
    const ledger = new Ledger({
        user_id: date,
        from_wallet:req.body.from_wallet,
        to_wallet:req.body.to_wallet,
        chain:req.body.chain,
        token:req.body.token,
        amount:req.body.amount,
        type:req.body.type,
        create_time: date,
        modify_time:req.body.modify_time,
    });
    Ledger.createTransaction(
        ledger, (err,data) => {
            if (err)
            res.status(500).send({
              message:
                err.message || "Some error occurred while creating"
            });
          else res.send(data);
        }
    )
    
  };
  exports.findAll = (req, res) => {
  
    Ledger.getAllTransactions((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving"
        });
      else res.send(data);
    });
  };