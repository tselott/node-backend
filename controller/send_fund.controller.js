const axios = require('axios');
const SendFundToBank = require('../models/send_fund.model')
const config = require("../config/config.js");
const { v4: uuidv4 } = require('uuid');


exports.create = (req, res) => {
    if (!req.body) {
        res.status(400).send({
          message: "Content can not be empty!"
        });
    }
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+ config.API_KEY
      }
    axios.post('https://api-sandbox.circle.com/v1/payouts', 
    {
        "idempotencyKey":uuidv4(),
        "source":{
           "type":"wallet",
           "id":req.body.from_wallet
        },
        "destination":{
           "type":"wire",
           "id":req.body.to_bank 
        },
        "amount":{
           "currency":"USD",
           "amount":req.body.amount
        },
        "metadata":{
           "beneficiaryEmail":req.body.email
        }
     }
      ,{headers:headers})
      .then(function (response) {
          console.log(response.data);
        //   res.json(response.data);
          const send_fund = new SendFundToBank({
              transaction_id:response.data.data.id,
              from_wallet:response.data.data.sourceWalletId,
              to_bank:response.data.data.destination.id,
              bank_account_details:JSON.stringify(response.data.data.destination)
            });
            SendFundToBank.createTransaction(
                send_fund, (err,data) => {
                    if (err)
                    res.status(500).send({
                        message:
                        err.message || "Some error occurred while creating"
                    });
                    else res.json(data);
                }
            )
      })
      .catch(function (error) {
        console.log(error);
        res.json(error);
      });
  };
  exports.findAll = (req, res) => {
  
    SendFundToBank.getAllTransactions((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving"
        });
      else res.send(data);
    });
  };